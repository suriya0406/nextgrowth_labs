from django.urls import path
from . import views

urlpatterns = [
    path('create_user/', views.create_user, name="create_user"),
    path('login_user/', views.login, name="login_user"),
    path('logout_user/', views.logout_user, name="logout_user"),
    path('get_user/', views.GetUserAPI.as_view(), name="get_user"),
    path('get_all_user/', views.GetAllUserAPI.as_view(), name="get_all_user"),
    path('user_update/<int:id>', views.UserUpdateAPI.as_view(), name="user_update"),
    path('user_delete/<int:id>', views.UserDeleteAPI.as_view(), name="user_delete"),

]

