from django.contrib.auth.models import User
from rest_framework.response import Response
from . serializers import UserSerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

@api_view(['GET', 'POST'])
def create_user(request):
    
    if request.method == 'GET':
        
        try:
            user = User.objects.all()
            serializer = UserSerializer(user, many=True)
            return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif request.method == 'POST':
        try:
            data = {
                'first_name': request.data['first_name'],
                'last_name'  : request.data['last_name'],
                'username'  : request.data['username'],
                'email'    : request.data['email'],
                'password'    :request.data['password'],
                }  
            serializer = UserSerializer(data=data)
            if serializer.is_valid():  
                serializer.save()
                return Response(serializer.data , status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)  

@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    try:
        username = request.data.get("username")
        password = request.data.get("password")
        if username is None or password is None:
            return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
        user = authenticate(username=username, password=password)
        if not user:
            return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
        token, _ = Token.objects.get_or_create(user=user)
        serializer = UserSerializer(user)
        return Response({
            'user_info':serializer.data, 
            'token': token.key,
            },
            status=HTTP_200_OK)
    except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)  

@api_view(["POST"])
@permission_classes([IsAuthenticated])
def logout_user(request):
    try:
        user = request.user
        print(user)
        user.auth_token.delete()
        # logout(request)
        return Response({'message':'Logged out successfully'}, status=status.HTTP_200_OK)

    except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_401_UNAUTHORIZED)  

@api_view(['GET'])
class GetUserAPI(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        try:
            user = self.request.user
            print(user)
            serializer = UserSerializer(user)
            return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetAllUserAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self,request):
        try:
            users = User.objects.all()
            serializer = UserSerializer(users, many=True)
            return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['GET', 'POST'])
class UserUpdateAPI(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, id):
        
        try:
            user = User.objects.get(id=id)  
            serializer = UserSerializer(user)
            return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)

        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        

    def post(self, request, id):

        try:
            data = {
                'first_name': request.data['first_name'],
                'last_name'  : request.data['last_name'],
                'username'  : request.data['username'],
                'email'    : request.data['email'],
                }       
            print(data)
            user = User.objects.get(id=id)  
            serializer = UserSerializer(user, data=data, partial = True)
            if serializer.is_valid():  
                serializer.save()
                return Response({'message':'User details updated successfully','user_info':serializer.data} , status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['DELETE'])
class UserDeleteAPI(APIView):

    permission_classes = (IsAuthenticated, )
    
    def delete(self, request, id):  
        try:
            user = User.objects.get(id=id)  
            user.delete()  
            return Response({'message':'User details deleted successfully'}, status=status.HTTP_200_OK)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
